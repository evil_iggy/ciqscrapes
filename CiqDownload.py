from bs4 import BeautifulSoup
import sys
import os, errno
import shutil
import time
from dateutil.parser import parse
from selenium import webdriver
import pymssql


'''
Author: Bryson Hirai-Hadley
Date: 20170712
Purpose: download transcripts of earnings calls for all companies we are concerned with after they release their 
Change Log:
'''
'''
TODO: WRITE TO A LOG FILE
'''

def is_date(string):
    try: 
        parse(string)
        return True
    except ValueError:
        return False

def updateLastAttempt(CompanyId):
        query = """update reportconfig.CiqDownloadTimestamp \n
                   set LastAttemptWNoTranscript = GETDATE() \n"""
        query += "where CompanyId = "+str(CompanyId)
        
        conn = pymssql.connect("EC-SERVER1",user,password,"Skynet")
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        conn.close()
    

last_date = parse('2017-04-01')

loginUrl = 'https://www.capitaliq.com/ciqdotnet/login-sso.aspx?contextType=external&username=string&enablePersistentLogin=true&OverrideRetryLimit=0&contextValue=%2Foam&password=secure_string&challenge_url=https%3A%2F%2Fwww.capitaliq.com%2Fciqdotnet%2Flogin-sso.aspx&request_id=8518211414115771760&authn_try_count=0&locale=en_US&resource_url=https%253A%252F%252Fwww.capitaliq.com%252Fciqdotnet%252Flogin.aspx%253Fredirect%253D%25252fCIQDotNet%25252fmy%25252fdashboard.aspx'
loginUrl = 'https://www.capitaliq.com/CIQDotNet/my/dashboard.aspx'
payload = {'username' : 'hadley@englishcap.com',
           'password' : 'Pa0deQue'}

myDriver = webdriver.Chrome('C:\\Users\\hadle\\AppData\\Local\\ChromeDriver\\chromedriver.exe')
myDriver.get(loginUrl)

page = myDriver.page_source
soup = BeautifulSoup(page, "lxml")

uidBox = myDriver.find_element_by_id("username")
pwdBox = myDriver.find_element_by_id("password")

uidBox.send_keys("hadley@englishcap.com")
pwdBox.send_keys("Pa0deQue")

myDriver.find_element_by_id("myLoginButton").click()

#get the list of securities from the db
companyIds = []
gvkeys = []
tickers = []
updateDates = []
query = 'exec report.spGetCompaniesForCiqScrape'
connectionStr = "Persist Security Info=False;Integrated Security=false;Initial Catalog=Skynet;server=EC-SERVER1"
user = "datareader"
password = "Phalanx"
conn = pymssql.connect("EC-SERVER1",user,password,"Skynet")
cursor = conn.cursor()
cursor.execute(query)
i=0

for row in cursor:
    companyIds.append(row[0])
    gvkeys.append(row[1])
    tickers.append(row[2])
    updateDates.append(row[3])
    #print(tickers[i]+": "+str(updateDates[i]))
    
    i += 1
    
conn.close()

time.sleep(6)
downloadsFolder = 'C:\\Users\\hadle\\Downloads\\'
destBase = 'F:\\Website Docs\\Company_'
urlBase = 'https://www.capitaliq.com/CIQDotNet/Transcripts/Summary.aspx?CompanyId='
stg = "thing"
illegalChars = ['/',':','?','!']
#get the transacripts, one at a time


i=0
for cid in companyIds:
    lastDate = updateDates[i]
    print("getting data for "+tickers[i]+": "+str(lastDate))
    url = urlBase + str(cid)
    destFolder = destBase+gvkeys[i]+'\\'
    #print(url)
    
    myDriver.get(url)
    page = myDriver.page_source
    soup = BeautifulSoup(page, "lxml")
    table = soup.findAll("table", {"class" : "cTblListBody"})[0]
    counter = 0
    foundDocs = False
    #check if no data exits
    if len(table.find_all('tr')) < 2:
        updateLastAttempt(cid)
    
    for row in table.find_all('tr')[1:]:
        col = row.find_all('td')
        if len(col) > 3:
            if is_date(col[1].get_text()):
                dt = parse(col[1].get_text())
                if dt > last_date:
                    print(str(dt)+" is after last date of "+str(lastDate))
                    stg = col[3].get_text().strip()
                    fileName = col[2].get_text().strip()+'.pdf'
                    #work out the kinks
                    if '\n' in fileName:
                        fileName = fileName.split('\n')[0]
                    for iChar in illegalChars:
                        if iChar in fileName:
                            fileName = fileName.replace(iChar,'-')

                    #rule that filename is col[3] does not hold for M&A calls... use this rule instead
                    if 'Participating Companies' in fileName:
                        loc = fileName.find('Participating Companies')
                        fileName = fileName[:loc]+".pdf"
                    print(fileName)
                    if stg == 'Shareholder/Analyst Call':
                        print("sac")
                    elif stg == 'Earnings Call':
                        print("ec")
                    links = col[4].findAll('a', {"class" : "binderIcoSprite_docType_PDF"})
                    if len(links) > 0:
                        for link in links:
                            myDriver.find_element_by_id(link['id']).click(); #goes to my downloads...   
                            time.sleep(3.5) #wait for the download to complete!
                            #move the file
                            try:
                                if not os.path.exists(destFolder):
                                    os.makedirs(destFolder)
                                if not os.path.isfile(downloadsFolder+fileName):
                                    time.sleep(2)
                                if os.path.isfile(downloadsFolder+fileName):
                                    shutil.move(downloadsFolder+fileName,destFolder+fileName)
                                    print("saved: "+destFolder+fileName)
                                else: 
                                    #write to log file
                                    print("file not found: "+downloadsFolder+fileName)
                                #time.sleep(1)

                                
                            except OSError as e:
                                if e.errno != errno.EEXIST:
                                    raise
                                    myDriver.close()
                                    myDriver.quit()
                            
                                                        #update the db
                            #the first row of the table will be the latest file
                            if counter == 0:
                                query = """update reportconfig.CiqDownloadTimestamp \n
                                           set LastDownloadedFile = '"""
                                query += str(dt)+"""' \n """
                                query += "where CompanyId = "+str(cid)
                                
                                conn = pymssql.connect("EC-SERVER1",user,password,"Skynet")
                                cursor = conn.cursor()
                                cursor.execute(query)
                                conn.commit()
                                conn.close()
                            counter += 1
                elif (last_date - dt).days > 180: #no recent data at all... don't try again tomorrow!
                    updateLastAttempt(cid)
                 
            else:
                print("no date in column 1")
                print(col[1])
    i += 1


myDriver.close()
myDriver.quit()

print("Eu tou all done")
sys.exit(0)
